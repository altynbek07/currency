# Currency

Service providing currency rates.

## Getting started

### Prerequisites

- PHP 7.4
- Node 10.19.0

### Installing

#### Manual

```bash
# Clone the project and run composer
git clone https://gitlab.com/altynbek07/currency.git
cd currency
composer install
cp .env.example .env

# Set your environment values in .env file
DB_DATABASE=db_currency
DB_USERNAME=root
DB_PASSWORD=
SANCTUM_STATEFUL_DOMAINS=localhost,127.0.0.1,currency.test

# Set the application key
php artisan key:generate

# Migration and DB seeder
php artisan migrate --seed

# Install dependency with NPM
npm install

# develop
npm run production # you can use `npm run dev` for develop or `npm run watch` for life-reload

# Run server
php artisan serve
```

#### Default user
If you run the migration command with seed `php artisan migrate --seed`, you will have an admin user:

- **Email**: admin@laravue.dev
- **Password**: laravue

#### Docker

```bash
# Clone the project
git clone https://gitlab.com/altynbek07/currency.git
cd currency

# You should copy these config files
cp ./docker/nginx/conf.d/app.conf.example ./docker/nginx/conf.d/app.conf
cp ./docker/mysql/my.cnf.example ./docker/mysql/my.cnf
cp .env.example .env

# Set your environment values in .env file
DB_HOST=currency_db
DB_DATABASE=db_currency
DB_USERNAME=root
DB_PASSWORD=root

# Run docker
docker-compose up -d
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate --seed
docker-compose exec app npm install
docker-compose exec app npm run production
```

Open http://localhost:80 (laravel container port declared in `docker-compose.yml`) to access project. Default login and password [here](#default-user).

## Commands
Importing currency rates
```sh
php artisan currency:import
```

Create new admin user
```sh
php artisan user:create admin@admin.com 123456
```

## Running the tests

- Tests system is under development

## Built with

- [Laravel](https://laravel.com/) - The PHP Framework For Web Artisans
- [Laravel Sanctum](https://github.com/laravel/sanctum/) - Laravel Sanctum provides a featherweight authentication system for SPAs and simple APIs.
- [spatie/laravel-permission](https://github.com/spatie/laravel-permission) - Associate users with permissions and roles.
- [VueJS](https://vuejs.org/) - The Progressive JavaScript Framework
- [Element](https://element.eleme.io/) - A Vue 2.0 based component library for developers, designers and product managers
- [Vue Admin Template](https://github.com/PanJiaChen/vue-admin-template) - A minimal vue admin template with Element UI
