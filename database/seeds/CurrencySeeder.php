<?php

use App\Models\Currency;
use App\Models\CurrencyLog;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Currency::class, 10)->create()->each(function ($currency) {
            $currency->currencyLogs()->saveMany(factory(CurrencyLog::class, 50)->make());
        });
    }
}
