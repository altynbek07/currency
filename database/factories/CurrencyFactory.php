<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Currency;
use Illuminate\Support\Arr;
use Faker\Generator as Faker;

$factory->define(Currency::class, function (Faker $faker) {
    return [
        'owner_id' => $faker->unique()->randomNumber(5),
        'num_code' => $faker->numberBetween(100, 999),
        'char_code' => $faker->currencyCode,
        'nominal' => $faker->randomElement([1, 10, 100]),
        'name' => $faker->words(2, true),
    ];
});
