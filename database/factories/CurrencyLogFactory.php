<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CurrencyLog;
use Faker\Generator as Faker;

$factory->define(CurrencyLog::class, function (Faker $faker) {
    $datetime = $faker->dateTimeThisMonth();
    return [
        'value' => $faker->randomFloat(4, 1, 200),
        'created_at' => $datetime,
        'updated_at' => $datetime
    ];
});
