// import moment from 'moment';

export default {
  methods: {
    handleDelete(id) {
      this.$confirm(this.$t('message.delete_before'), this.$t('message.warning'), {
        confirmButtonText: 'OK',
        cancelButtonText: this.$t('table.cancel'),
        type: 'warning',
      }).then(() => {
        this.resource.destroy(id).then(response => {
          this.$message({
            type: 'success',
            message: this.$t('message.delete_success'),
          });
          this.searchElements();
        }).catch(error => {
          console.log(error);
        });
      }).catch(() => {
        this.$message({
          type: 'info',
          message: this.$t('message.delete_cancel'),
        });
      });
    },
    async getList() {
      this.listLoading = true;
      const data = await this.resource.list(this.listQuery);
      this.list = data.data;
      this.total = data.meta.total;
      this.listLoading = false;
    },
    toggle(id, attribute) {
      this.resource.toggle(id, { 'attribute': attribute });
    },
    searchElements() {
      this.listQuery.page = 1;
      this.getList();
    },
  },
  filters: {
    // localDate(value) {
    //   if (value == null) {
    //     return 'Дата не указана';
    //   }
    //   return moment(value).format('DD.MM.YYYY');
    // },
  },
};
