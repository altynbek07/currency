import request from '@/utils/request';
import Resource from '@/api/resource';

class CurrencyResource extends Resource {
  constructor() {
    super('currencies');
  }

  logs(id, query) {
    return request({
      url: '/' + this.uri + '/' + id + '/logs',
      method: 'get',
      params: query,
    });
  }
}

export { CurrencyResource as default };
