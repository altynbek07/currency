/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const currencyRoutes = {
  path: '/currencies',
  component: Layout,
  redirect: '/currencies/list',
  name: 'Currency',
  meta: {
    title: 'currency',
    icon: 'icon-form',
    roles: ['admin'],
  },
  children: [
    {
      path: 'list',
      component: () => import('@/views/currency/List'),
      name: 'CurrencyList',
      meta: { title: 'currencyList', icon: 'dollar', noCache: true },
    },
    {
      path: ':id(\\d+)',
      component: () => import('@/views/currency/View'),
      name: 'CurrencyView',
      meta: { title: 'currencyView', noCache: true },
      hidden: true,
    },
    {
      path: 'edit/:id(\\d+)',
      component: () => import('@/views/currency/Edit'),
      name: 'CurrencyEdit',
      meta: { title: 'currencyEdit', noCache: true },
      hidden: true,
    },
    {
      path: 'create',
      component: () => import('@/views/currency/Create'),
      name: 'CurrencyCreate',
      meta: { title: 'currencyCreate', noCache: true },
      hidden: true,
    },
  ],
};

export default currencyRoutes;
