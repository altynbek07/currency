<?php

namespace App\Models\ModelFilters;

use EloquentFilter\ModelFilter;

class CurrencyFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function name($value)
    {
        return $this->where('name', 'LIKE', "%$value%");
    }

    public function charCode($value)
    {
        return $this->where('char_code', 'LIKE', "%$value%");
    }
}
