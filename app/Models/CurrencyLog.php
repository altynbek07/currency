<?php

namespace App\Models;

use DateTimeInterface;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class CurrencyLog extends Model
{
    use Filterable;

    protected $fillable = [
        'currency_id', 'value'
    ];

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
