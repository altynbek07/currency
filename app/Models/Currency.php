<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use Filterable;

    protected $fillable = [
        'owner_id', 'num_code', 'char_code', 'nominal', 'name'
    ];

    public function currencyLogs()
    {
        return $this->hasMany('App\Models\CurrencyLog');
    }
}
