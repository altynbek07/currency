<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Support\Arr;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Client\PendingRequest;

class CurrencyImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importing currency rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = $this->getHttpClient()->get('http://www.cbr.ru/scripts/XML_daily.asp');

        $currencyResponse = $this->handleResponse($response);
        $currencies = Arr::get($currencyResponse, 'Valute', []);

        $currencyCollection = Currency::all();

        foreach ($currencies as $currency) {
            $currencyOwnerId = Arr::get($currency, '@attributes.ID');
            $currencyExist = $currencyCollection->contains('owner_id', $currencyOwnerId);

            if ($currencyExist) {
                $currencyModel = $currencyCollection->first(function ($item, $key) use ($currencyOwnerId) {
                    return $item->owner_id === $currencyOwnerId;
                });

                $this->saveCurrencyLog($currencyModel, $currency);
            } else {
                $currencyModel = Currency::create([
                    'owner_id' => $currencyOwnerId,
                    'num_code' => Arr::get($currency, 'NumCode'),
                    'char_code' => Arr::get($currency, 'CharCode'),
                    'nominal' => (int) Arr::get($currency, 'Nominal'),
                    'name' => Arr::get($currency, 'Name'),
                ]);

                $this->saveCurrencyLog($currencyModel, $currency);
            }
        }

        $this->info("Import completed");
    }

    /**
     * HTTP Client
     *
     * @return PendingRequest
     */
    protected function getHttpClient(): PendingRequest
    {
        return Http::acceptJson();
    }

    /**
     * Handling response
     *
     * @param \Illuminate\Http\Client\Response $response
     * @return array
     */
    protected function handleResponse(Response $response): array
    {
        $xmlResponse = simplexml_load_string($response->throw()->body());

        // JSON encode the XML, and then JSON decode to an array.
        return json_decode(json_encode($xmlResponse), true);
    }

    /**
     * Save currency log
     *
     * @param \App\Models\Currency $currencyModel
     * @param array $currency
     * @return void
     */
    protected function saveCurrencyLog(Currency $currencyModel, array $currency)
    {
        $currencyModel->currencyLogs()->create([
            'value' => (float) (str_replace(',', '.', Arr::get($currency, 'Value')))
        ]);
    }
}
