<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'owner_id' => 'required|string|max:8|unique:currencies,owner_id,' . $this->id,
            'num_code' => 'required|string|max:3',
            'char_code' => 'required|string|max:3',
            'nominal' => 'required|integer',
            'name' => 'required|string',
        ];
    }
}
