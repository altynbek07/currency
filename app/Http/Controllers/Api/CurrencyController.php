<?php

namespace App\Http\Controllers\Api;

use App\Models\Currency;
use App\Models\CurrencyLog;
use App\Http\Controllers\Controller;
use App\Http\Requests\CurrencyRequest;
use App\Http\Resources\CurrencyResource;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\CurrencyLogResource;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = (int) request()->query('limit', 20);
        $items = Currency::filter(request()->query())->latest()->paginateFilter($limit);
        return CurrencyResource::collection($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CurrencyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurrencyRequest $request)
    {
        $currency = Currency::create($request->all());

        return new CurrencyResource($currency);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Currency $currency
     * @return \Illuminate\Http\Response
     */
    public function show(Currency $currency)
    {
        return new CurrencyResource($currency);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CurrencyRequest  $request
     * @param \App\Models\Currency $currency
     * @return \Illuminate\Http\Response
     */
    public function update(CurrencyRequest $request, Currency $currency)
    {
        $currency->update($request->all());

        return new CurrencyResource($currency);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Currency $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        $currency->delete();

        return response('Deleted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logs($id)
    {
        $periodGroup = request()->query('period_group', 'day');

        $logs = CurrencyLog::whereHas('currency', function (Builder $query) use ($id) {
            $query->where('currency_id', $id);
        })->filter(request()->query())->latest()->get();

        // Группируем по дням или по часам
        $logs = $logs->unique(function ($item) use ($periodGroup) {
            return $periodGroup === 'day' ? $item['created_at']->format('Y-m-d') : $item['created_at']->format('Y-m-d H');
        });

        // Сортируем по возрастанию даты создания
        $logs = $logs->sortBy('created_at');

        // Сбрасываем индексы
        $logs = $logs->values();

        return new CurrencyLogResource($logs);
    }
}
